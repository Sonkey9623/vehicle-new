<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          $a=[
        ['name'=>"abc",'email'=>'admin@gmail.com','password'=>bcrypt(env('ADMIN_PASSWD'))]
        ];

        DB::table('admins')->insert($a);
    }
}
