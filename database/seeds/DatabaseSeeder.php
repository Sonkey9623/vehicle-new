<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Model::unguard();
         DB::statement('SET FOREIGN_KEY_CHECKS = 0');
         DB::table('admins')->truncate();
         $this->call(AdminSeeder::class);
         DB::table('vehicle_type')->truncate();
         $this->call(VehicleTypeSeeder::class);
         DB::statement('SET FOREIGN_KEY_CHECKS = 1');
         Model::reguard();
    }
}
