<?php

use Illuminate\Database\Seeder;

class VehicleTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          $a=[
        ['type'=>"Two-Wheeler"],
        ['type'=>"Four-Wheeler"],
        ];

        DB::table('vehicle_type')->insert($a);
    }
}
