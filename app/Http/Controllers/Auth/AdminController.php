<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\vehicletype;
use App\Savebrand;
class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$type=vehicletype::all();
        return view('admin',compact('type'));
    }

     public function savebrand(Request $request)
    {
    	  $brand=new Savebrand();

        $brand->name=$request->Typename;
        $brand->type_id=$request->type_id;
        // return $brand;

        $brand->save();
         // $request->session()->flash('status', 'Type Stored Successfully');
        return redirect()->back();
    }
}