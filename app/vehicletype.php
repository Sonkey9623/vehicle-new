<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class vehicletype extends Model
{
		 protected $table="vehicle_type";
     protected $fillable = [
        'type',
    ];
}
