<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Savebrand extends Model
{
    	 protected $table="brands";
     protected $fillable = [
        'name','type_id',
    ];
}
