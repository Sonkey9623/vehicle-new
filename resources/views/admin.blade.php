@extends('layouts.app')

@section('content')



    <section id="inner-headline">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="pageTitle">Add New Brands</h2>
            </div>
        </div>
    </div>
    </section>

<br>
        <div class="container">
        <div class="row">
        <div class="col-md-9">
            @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
          <form action="/admin/savebrand" method="post" enctype="multipart/form-data">
         {{csrf_field()}}
          
          
                <div class="control-group">
                    <div class="controls">
                    <h4>Brand Name</h4>
            <input type="text" class="form-control" 
                   placeholder="Typename" name="Typename" required
                       data-validation-required-message="Please enter Title name" />
              <p class="help-block"></p>
           </div>
             </div>  

                 @if(isset($type))
<h4>Vehicle Type</h4>

<b><i><select name="type_id">
@foreach($type as $t)
<option value="{{$t->id}}">{{$t->type}}</option>

@endforeach
</select></i></b><br>
@endif
              
           
               
         <br> <button type="submit" value="submit" class="btn btn-primary pull-left">Add Brand name</button></br><br>
          </form><br> 
         <hr>
         <!-- <h2>All Categories</h2><br>
<div class="table-responsive">          
  <table class="table">
    <thead>
      <tr>
        <th>No</th>
      <th>category Name</th>
      <th>Remove</th>
      <th>View Details</th>
       <th>Rename</th>
      </tr>
    </thead>
    <tbody>
       @isset($category)
       @foreach($category as $cat=>$c)
      <tr>
        <td>{{++$cat}}</td>
       <td> {{$c->category_name}}</td>
       
      <td><a href="/admin/delete/{{$c->id}}" class="btn btn-danger">Delete</a></td>
      <td><a href="/admin/subcat/{{$c->id}}" class="btn btn-info">Add Subcategory</a></td>
       <td><button type="button" class="btn btn-warning btn-lg" data-toggle="modal" data-target="#{{$c->id}}">Rename</button></td>
      </tr>
      <div class="modal fade" id="{{$c->id}}" role="dialog">
    <div class="modal-dialog">
     -->
      <!-- Modal content-->
      <!-- <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Rename Category</h4>
        </div>
        <div class="modal-body">
          <form action="/admin/update" method="post">
         {{csrf_field()}}
          
          <input type="hidden" name="catid" value="{{$c->id}}">
                <div class="control-group">
                    <div class="controls">
                    <h4>category Name</h4>
                    
            <input type="text" class="form-control" 
                   placeholder="categoryname" name="categoryname" value="{{$c->category_name}}" required
                       data-validation-required-message="Please enter Title name" />
              <p class="help-block"></p>
            
           </div>
             </div>
             <input type="submit" class="btn btn-primary" value="update" >
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          
        </div>
      </div> -->
      <!-- </div>
</div>
      @endforeach
     @endisset
    </tbody>
  </table>
  

          </div> -->
           </div>
            </div>
             </div>
       
      @endsection    
 

